#include <iostream>
#include <SFML/Graphics.hpp>
#include <chrono>
#include "gol.h"

#define WIDTH 1200
#define HEIGHT 1200

auto main() -> int {
    sf::RenderWindow app(sf::VideoMode(WIDTH, HEIGHT), "First try");
    app.setFramerateLimit(60);

    Board b{};

    while (app.isOpen()) {
        sf::Event event;
        while (app.pollEvent(event)) {
            if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
                app.close();
            }

            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::Space) {
                    b.toggle_pause();
                }
            }

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                auto pos = sf::Mouse::getPosition(app);
                if (pos.x >= 0 && pos.x < WIDTH && pos.y >= 0 && pos.y < HEIGHT)
                    b.handle_click(sf::Mouse::Left, b.coords_to_cell(app, sf::Mouse::getPosition(app)));
            } else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                auto pos = sf::Mouse::getPosition(app);
                if (pos.x >= 0 && pos.x < WIDTH && pos.y >= 0 && pos.y < HEIGHT)
                    b.handle_click(sf::Mouse::Right, b.coords_to_cell(app, sf::Mouse::getPosition(app)));
            }
        }

        app.clear(sf::Color::Black);
        b.draw(app);
        app.display();
    }

    return EXIT_SUCCESS;
}
