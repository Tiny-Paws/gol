#ifndef GOL_H
#define GOL_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <chrono>

enum class CellState {
    Alive,
    Dead
};

class Cell {
    CellState _state = CellState::Dead;
public:
    CellState State() const noexcept { return _state; }
    void SetState(CellState state) noexcept { _state = state; }
    void Toggle() noexcept { _state = _state == CellState::Alive ? CellState::Dead : CellState::Alive; }
    sf::Color Color() const noexcept {
        if (_state == CellState::Alive)
            return sf::Color::White;
        return sf::Color::Black;
    }
};

struct Neighbours {
    int dead = 0;
    int alive = 0;
};

class Board {
    std::vector<std::vector<Cell>> _board;
    std::chrono::time_point<std::chrono::steady_clock> _lastUpdate = std::chrono::steady_clock::now();
    bool _pause = false;

    void _update_state() {
        if(_pause)
            return;
        auto now = std::chrono::steady_clock::now();

        std::vector<std::reference_wrapper<Cell>> revive{};
        std::vector<std::reference_wrapper<Cell>> graveyard{};

        if (std::chrono::duration_cast<std::chrono::milliseconds>(now - _lastUpdate).count() > 100) {
            _lastUpdate = now;
            for (int i = 0; i < _board.size(); ++i) {
                for (int j = 0; j < _board[i].size(); ++j) {
                    auto n = _neighbours(j, i);
                    if (_board[i][j].State() == CellState::Alive) {
                        if (n.alive < 2 || n.alive > 3)
                            graveyard.push_back(_board[i][j]);

                    } else {
                        if (n.alive == 3)
                            revive.push_back(_board[i][j]);
                    }
                }
            }
            for (auto& cell : revive)
                cell.get().SetState(CellState::Alive);
            for (auto& cell : graveyard)
                cell.get().SetState(CellState::Dead);
        }
    }

    Neighbours  _neighbours(int x, int y) const noexcept {
        Neighbours neighbours{};
        int n[3]{-1, 0, 1};
        for (auto const& dy : n) {
            for (auto const& dx : n) {
                if (dy == 0 && dx == 0)
                    continue;
                try {
                    auto const status = _board.at(y + dy).at(x + dx);
                    if (status.State() == CellState::Alive) {
                        ++neighbours.alive;
                    } else {
                        ++neighbours.dead;
                    }
                }
                catch(std::out_of_range& const oor) {
                    ++neighbours.dead;
                    continue;
                }
            }
        }
        return neighbours;
    }

    int _size_of_cells(unsigned size, unsigned number_cells) const noexcept {
        return size / number_cells;
    }

public:
    Board(int x, int y) : _board(x, std::vector<Cell>(y)) {}
    Board(int x = 100) : Board(x,x) {}

    void draw(sf::RenderTarget& target) noexcept {
        _update_state();
        auto cell_y = _size_of_cells(target.getSize().y, _board.size());
        auto cell_x = _size_of_cells(target.getSize().x, _board[0].size());

        sf::RectangleShape cell{sf::Vector2f(cell_x, cell_y)};
        for (int i = 0; i < _board.size(); ++i) {
            for (int j = 0; j < _board[i].size(); ++j) {
                cell.setFillColor(_board[i][j].Color());
                cell.setPosition(j * cell_x, i * cell_y);
                target.draw(cell);
            }
        }
    }

    void toggle_pause() noexcept {
        _pause = !_pause;
    }

    void handle_click(sf::Mouse::Button button, Cell& cell) noexcept {
        auto cellState = button == sf::Mouse::Left ? CellState::Alive : CellState::Dead;
        cell.SetState(cellState);
    }

    Cell& coords_to_cell(sf::RenderTarget& target, sf::Vector2i coords) noexcept {
        auto cell_size_x = _size_of_cells(target.getSize().x, _board[0].size());
        auto cell_size_y = _size_of_cells(target.getSize().y, _board.size());
        auto x = coords.x / cell_size_x;
        auto y = coords.y / cell_size_y;

        return _board[y][x];
    }
};

#endif GOL_H
